<?php
namespace Composer\Installers;
class LightyInstaller extends BaseInstaller
{
    protected $locations = array(
        'plugin'    => 'plugins/{$name}/',
        'docs'      => '{$name}/',
        'atomium'      => 'vendor/lighty/kernel/src/Atomium/',
    );
}
